package br.com.itau;

import java.util.ArrayList;
import java.util.Random;

public class Sorteador {

    public static Carta sortearCarta(ArrayList<Carta> cartas){
        Random posicaoSorteada = new Random();
        int posicao = posicaoSorteada.nextInt(cartas.size());

        Carta carta = new Carta(cartas.get(posicao).getIdentificadorCarta(),cartas.get(posicao).getValor(),cartas.get(posicao).getNaipe());

        return carta;
    }
}
