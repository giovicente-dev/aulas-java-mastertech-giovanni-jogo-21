package br.com.itau;

public class Carta {
    private int identificadorCarta;
    private int valor;
    private NaipesEnum naipe;

    public Carta(){}

    public int getIdentificadorCarta() {
        return identificadorCarta;
    }

    public int getValor() {
        return valor;
    }

    public NaipesEnum getNaipe() {
        return naipe;
    }

    public Carta(int identificadorCarta, int valor, NaipesEnum naipe){
        this.identificadorCarta = identificadorCarta;
        this.valor = valor;
        this.naipe = naipe;
    }
}
