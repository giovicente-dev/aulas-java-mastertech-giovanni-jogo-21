package br.com.itau;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        boolean iniciar;
        Carta carta = new Carta();
        int contadorRodadas = 1;
        int pontos = 0;
        Rodada rodada = new Rodada();

        do {
            Menus.apresentarMenuInicial();
            int opcao = Leitor.lerOpcao();
            iniciar = OrquestradorJogo.iniciarJogo(opcao);
        } while (!iniciar);

        ArrayList<Carta> cartas = new ArrayList();
        ArrayList<Carta> cartasSorteadas = new ArrayList();

        cartas = Baralho.iniciarBaralho();

        int opcao = 1;

        do {

            Menus.apresentarOpcoesDeJogo();
            opcao = Leitor.lerOpcao();

            if (opcao == 1) {
                carta = OrquestradorJogo.executarJogo(cartas, cartasSorteadas);
                pontos = pontos + rodada.verificarPontuacao(carta);

                System.out.println("Você tirou um " + carta.getValor() + " DE " + carta.getNaipe());
                System.out.println("Você tem " + pontos + " pontos.");

                if(pontos > 21){
                    System.out.println("Você perdeu o jogo =(");
                    opcao = 2;
                } else if (pontos == 21){
                    System.out.println("Parabéns!!! Você fez 21 pontos =)");
                    opcao = 2;
                }
            }

        } while (opcao == 1);

        if (pontos >= 0 && pontos < 21){
            System.out.println("Você encerrou o jogo com " + pontos + " pontos!");
        }

        rodada.setNumero(contadorRodadas);
        rodada.setPontos(pontos);

        contadorRodadas++;

    }

}
