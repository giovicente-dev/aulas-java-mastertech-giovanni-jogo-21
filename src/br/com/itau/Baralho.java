package br.com.itau;

import java.util.ArrayList;

public class Baralho {

    public Baralho() {
    }

    public static ArrayList<Carta> iniciarBaralho() {
        ArrayList<Carta> cartas = new ArrayList();
        NaipesEnum naipe = null;

        for (int i = 0; i < 4; i++) {
            switch (i) {
                case 0:
                    naipe = NaipesEnum.COPAS;
                    break;

                case 1:
                    naipe = NaipesEnum.OUROS;
                    break;

                case 2:
                    naipe = NaipesEnum.PAUS;
                    break;

                case 3:
                    naipe = NaipesEnum.ESPADAS;
            }
            for (int j = 0; j < 12; j++) {
                if (j < 9) {
                    Carta carta = new Carta((j + 1), (j + 1), naipe);
                    cartas.add(carta);
                } else {
                    Carta carta = new Carta((j + 1), 10, naipe);
                    cartas.add(carta);
                }
            }
        }
        return cartas;
    }

    public static boolean verificarCartasSorteadas(Carta carta, ArrayList<Carta> cartasSorteadas) {
        for (int i = 0; i < cartasSorteadas.size(); i++) {
            if ((carta.getIdentificadorCarta() == cartasSorteadas.get(i).getIdentificadorCarta()) &&
                (carta.getValor() == cartasSorteadas.get(i).getValor()) &&
                (carta.getNaipe().equals(cartasSorteadas.get(i).getNaipe()))) {
                return true;
            }
        }
        return false;
    }

    public static void adicionarCartaSorteada(Carta carta, ArrayList<Carta> cartasSorteadas) {
        cartasSorteadas.add(carta);
    }
}
