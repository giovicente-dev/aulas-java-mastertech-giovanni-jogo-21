package br.com.itau;

import java.util.Scanner;

public class Leitor {
    private static Scanner scanOpcao;

    public static int lerOpcao(){
        scanOpcao = new Scanner(System.in);
        int opcao = scanOpcao.nextInt();
        return opcao;
    }
}
