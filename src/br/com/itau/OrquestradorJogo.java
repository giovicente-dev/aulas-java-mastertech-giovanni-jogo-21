package br.com.itau;

import java.util.ArrayList;

public class OrquestradorJogo {

    public static boolean iniciarJogo(int opcao) {
        if (opcao == 1) {
            return true;
        }
        return false;
    }

    public static Carta executarJogo(ArrayList<Carta> cartas, ArrayList<Carta> cartasSorteadas) {
        Carta carta = new Carta();
        carta = Sorteador.sortearCarta(cartas);

        while (Baralho.verificarCartasSorteadas(carta, cartasSorteadas)) {
            carta = Sorteador.sortearCarta(cartas);
        }
        cartasSorteadas.add(carta);
        return carta;

    }
}
