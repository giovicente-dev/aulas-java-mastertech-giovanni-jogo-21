package br.com.itau;

public class Rodada {
    private int numero, pontos;

    public Rodada(){}

    public Rodada(int numero) {
        this.numero = numero;
    }

    public Rodada(int numero, int pontos) {
        this.numero = numero;
        this.pontos = pontos;
    }

    public int getNumero() {
        return numero;
    }

    public int getPontos() {
        return pontos;
    }

    public void setNumero(int numero){
        this.numero = numero;
    }

    public void setPontos(int pontos) {
        this.pontos = pontos;
    }

    public int verificarPontuacao(Carta carta){
        return carta.getValor();

       /* if(rodada.pontos == 21){
            System.out.println("Parabéns! Você ganhou o jogo!");
        } else if(rodada.pontos < 21){
            System.out.println("Você tem " + rodada.pontos + " pontos.");
        } else {
            System.out.println("Você ultrapassou 21 pontos =(");
        }
        return rodada.pontos; */
    }
}
