package br.com.itau;

public class Menus {

    public static void apresentarMenuInicial(){
        System.out.println("********** Jogo 21 **********");
        System.out.println("Aperte 1 + Enter para iniciar");
    }

    public static void apresentarOpcoesDeJogo(){
        System.out.println("Opções");
        System.out.println("1 - Comprar Carta");
        System.out.println("2 - Parar");
    }

    public static void apresentarMenuContinuacao(){
        System.out.println("1 - Sim");
        System.out.println("2 - Não");
    }
}
